### A simple Rest API to register some students

#### How to run

First run the PostgreSQL docker container,  run the following command in project root folder:

```
$ docker-compose up -d
``` 

To chek if the docker container is running

```
$ docker-compose ps
```

```
        Name                       Command              State           Ports          
--------------------------------------------------------------------------------------
postgres-myacademyapi   docker-entrypoint.sh postgres   Up      0.0.0.0:5432->5432/tcp 
```

Build this

```
$ ./gradlew clean build
``` 

Run

```
$ ./gradlew clean bootRun
``` 

Alternatively you can run like this

```
$ java -jar build/libs/myacademyapp-0.0.1-SNAPSHOT.jar 
``` 

To stop the docker container

```
$ docker-compose down 
``` 

