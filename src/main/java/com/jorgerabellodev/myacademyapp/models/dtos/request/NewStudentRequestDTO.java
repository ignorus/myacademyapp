package com.jorgerabellodev.myacademyapp.models.dtos.request;

import java.util.Objects;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class NewStudentRequestDTO {

  @NotEmpty(message = "The name field is required")
  private String name;
  @Email(message = "Please, send a valid e-mail address")
  @NotEmpty(message = "The e-mail field is required")
  private String email;

  public NewStudentRequestDTO() {
  }

  public NewStudentRequestDTO(
      @NotEmpty(message = "The name field is required") String name,
      @Email(message = "Please, send a valid e-mail address") @NotEmpty(message = "The e-mail field is required") String email) {
    this.name = name;
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "NewStudentRequestDTO{" +
        "name='" + name + '\'' +
        ", email='" + email + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NewStudentRequestDTO that = (NewStudentRequestDTO) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(email, that.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, email);
  }
}
