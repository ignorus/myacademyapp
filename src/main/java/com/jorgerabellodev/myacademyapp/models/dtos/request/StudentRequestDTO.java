package com.jorgerabellodev.myacademyapp.models.dtos.request;

import java.util.Objects;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class StudentRequestDTO {

  private Long id;
  @NotEmpty(message = "The name field is required")
  private String name;
  @Email(message = "Please, send a valid e-mail address")
  @NotEmpty(message = "The e-mail field is required")
  private String email;

  public StudentRequestDTO() {
  }

  public StudentRequestDTO(Long id,
      @NotEmpty(message = "The name field is required") String name,
      @Email(message = "Please, send a valid e-mail address") @NotEmpty(message = "The e-mail field is required") String email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "StudentRequestDTO{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StudentRequestDTO that = (StudentRequestDTO) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(name, that.name) &&
        Objects.equals(email, that.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, email);
  }
}
