package com.jorgerabellodev.myacademyapp.models.dtos.response;

import java.util.List;
import java.util.Objects;

public class APILinks {

  private List<String> links;

  public APILinks() {
  }

  public APILinks(List<String> links) {
    this.links = links;
  }

  public List<String> getLinks() {
    return links;
  }

  public void setLinks(List<String> links) {
    this.links = links;
  }

  @Override
  public String toString() {
    return "APILinks{" +
        "links=" + links +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    APILinks apiLinks = (APILinks) o;
    return Objects.equals(links, apiLinks.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(links);
  }
}
