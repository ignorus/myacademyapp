package com.jorgerabellodev.myacademyapp.controllers.api;

import com.jorgerabellodev.myacademyapp.models.dtos.request.NewStudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.request.StudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.response.StudentResponseDTO;
import com.jorgerabellodev.myacademyapp.services.IStudentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

  private final IStudentService service;

  @Autowired
  public StudentController(IStudentService service) {
    this.service = service;
  }

  @ApiOperation(
      value = "Add a new Student",
      notes = "Return the created student",
      response = NewStudentRequestDTO.class,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created a new student"),
      @ApiResponse(code = 400, message = "Field validation error"),
      @ApiResponse(code = 400, message = "Bad bad server..."),
      @ApiResponse(code = 409, message = "This student is already registered"),
      @ApiResponse(code = 415, message = "Unsuported media type, change your Content-Type header to application/json")
  })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> save(
      @Valid
      @RequestBody NewStudentRequestDTO newStudentRequestDTO,
      @RequestHeader(
          value = "Content-Type",
          required = true,
          name = "Content-Type",
          defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType) {
    return new ResponseEntity<>(service.save(newStudentRequestDTO), HttpStatus.CREATED);
  }

  @ApiOperation(
      value = "Retrieve all registered students",
      notes = "Retrieve all registered students",
      response = StudentResponseDTO.class,
      responseContainer = "List",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "See all registered students"),
      @ApiResponse(code = 204, message = "There's no registered students yet"),
      @ApiResponse(code = 400, message = "Field validation error"),
      @ApiResponse(code = 400, message = "Bad bad server..."),
      @ApiResponse(code = 415, message = "Unsuported media type, change your Content-Type header to application/json")
  })
  @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAll(
      @RequestHeader(
          value = "Content-Type",
          required = true,
          name = "Content-Type",
          defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType) {
    return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
  }

  @ApiOperation(
      value = "Retrieve a student based on his/her id",
      notes = "Retrieve a student based on his/her id",
      response = StudentResponseDTO.class,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "See the student register"),
      @ApiResponse(code = 404, message = "Student not found"),
      @ApiResponse(code = 415, message = "Unsuported media type, change your Content-Type header to application/json")
  })
  @GetMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findById(
      @PathVariable("id") Long id,
      @RequestHeader(
          value = "Content-Type",
          required = true,
          name = "Content-Type",
          defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType) {
    return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
  }

  @ApiOperation(
      value = "Delete a student based on his/her id",
      notes = "Delete a student based on his/her id",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiResponses(value = {
      @ApiResponse(code = 204, message = "Student successfuly deleted"),
      @ApiResponse(code = 404, message = "Student not found"),
      @ApiResponse(code = 415, message = "Unsuported media type, change your Content-Type header to application/json")
  })
  @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> delete(
      @PathVariable("id") Long id,
      @RequestHeader(
          value = "Content-Type",
          required = true,
          name = "Content-Type",
          defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @ApiOperation(
      value = "Update a student data based on his/her id",
      notes = "Update a student data based on his/her id",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Student successfuly updated"),
      @ApiResponse(code = 404, message = "Student not found"),
      @ApiResponse(code = 415, message = "Unsuported media type, change your Content-Type header to application/json")
  })
  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> update(
      @RequestBody StudentRequestDTO studentRequestDTO,
      @RequestHeader(
          value = "Content-Type",
          required = true,
          name = "Content-Type",
          defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType) {
    return new ResponseEntity<>(service.update(studentRequestDTO), HttpStatus.OK);
  }

}
