package com.jorgerabellodev.myacademyapp.errors.handlers;

import com.jorgerabellodev.myacademyapp.errors.details.AlreadyRegistredStudentDetails;
import com.jorgerabellodev.myacademyapp.errors.details.AlreadyRegistredStudentDetails.Builder;
import com.jorgerabellodev.myacademyapp.errors.details.EmptyStudentDataSetDetails;
import com.jorgerabellodev.myacademyapp.errors.details.ErrorDetail;
import com.jorgerabellodev.myacademyapp.errors.details.ResourceNotFoundDetails;
import com.jorgerabellodev.myacademyapp.errors.details.ValidationErrorDetails;
import com.jorgerabellodev.myacademyapp.errors.exceptions.AlreadyRegistredStudentException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.EmptyStudentDataSetException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.ResourceNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(AlreadyRegistredStudentException.class)
  public ResponseEntity<?> handleAlreadyRegistredStudentException(
      AlreadyRegistredStudentException arsException) {
    AlreadyRegistredStudentDetails arsExceptionDetails = Builder.newBuilder()
        .timestamp(new Date().getTime())
        .status(HttpStatus.CONFLICT.value())
        .title("This student is already registered")
        .detail(arsException.getMessage())
        .developerMessage(arsException.getClass().getName())
        .build();

    return new ResponseEntity<>(arsExceptionDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(EmptyStudentDataSetException.class)
  public ResponseEntity<?> handleEmptyStudentDataSetException(
      EmptyStudentDataSetException esdException) {
    EmptyStudentDataSetDetails esdExceptionDetails = EmptyStudentDataSetDetails.Builder.newBuilder()
        .timestamp(new Date().getTime())
        .status(HttpStatus.NO_CONTENT.value())
        .title("There's no students here")
        .detail(esdException.getMessage())
        .developerMessage(esdException.getClass().getName())
        .build();

    return new ResponseEntity<>(esdExceptionDetails, HttpStatus.NO_CONTENT);

  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException rnfException) {
    ResourceNotFoundDetails rfnExceptionDetails = ResourceNotFoundDetails.Builder.newBuilder()
        .timestamp(new Date().getTime())
        .status(HttpStatus.NOT_FOUND.value())
        .title("Resource not found")
        .detail(rnfException.getMessage())
        .developerMessage(rnfException.getClass().getName())
        .build();

    return new ResponseEntity<>(rfnExceptionDetails, HttpStatus.NOT_FOUND);
  }

  @Override
  public ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException manvException,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {

    List<FieldError> fieldErrors = manvException.getBindingResult().getFieldErrors();

    String violatedFields = fieldErrors.stream()
        .map(FieldError::getField)
        .collect(Collectors.joining(","));

    String fieldMessage = fieldErrors.stream()
        .map(FieldError::getDefaultMessage)
        .collect(Collectors.joining(","));

    ValidationErrorDetails vedExceptionDetails = ValidationErrorDetails.Builder.newBuilder()
        .timestamp(new Date().getTime())
        .status(HttpStatus.BAD_REQUEST.value())
        .title("Field validation error")
        .detail("Field validation error")
        .developerMessage(manvException.getClass().getName())
        .field(violatedFields)
        .fieldMessage(fieldMessage)
        .build();

    return new ResponseEntity<>(vedExceptionDetails, HttpStatus.BAD_REQUEST);
  }

  @Override
  public ResponseEntity<Object> handleExceptionInternal(
      Exception ex,
      @Nullable Object body,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {

    ErrorDetail errorDetail = ErrorDetail.ErrorDetailBuilder.newBuilder()
        .timestamp(new Date().getTime())
        .status(status.value())
        .title("Bad bad server...")
        .detail(ex.getMessage())
        .developerMessage(ex.getClass().getName())
        .build();

    return new ResponseEntity<>(errorDetail, headers, status);
  }

}
