package com.jorgerabellodev.myacademyapp.errors.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AlreadyRegistredStudentException extends RuntimeException {

  public AlreadyRegistredStudentException(String message) {
    super(message);
  }

}
