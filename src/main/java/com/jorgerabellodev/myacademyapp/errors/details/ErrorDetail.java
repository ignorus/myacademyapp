package com.jorgerabellodev.myacademyapp.errors.details;

public class ErrorDetail {

  private String title;
  private Integer status;
  private String detail;
  private Long timestamp;
  private String developerMessage;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public Long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  public String getDeveloperMessage() {
    return developerMessage;
  }

  public void setDeveloperMessage(String developerMessage) {
    this.developerMessage = developerMessage;
  }


  public static final class ErrorDetailBuilder {

    private String title;
    private Integer status;
    private String detail;
    private Long timestamp;
    private String developerMessage;

    private ErrorDetailBuilder() {
    }

    public static ErrorDetailBuilder newBuilder() {
      return new ErrorDetailBuilder();
    }

    public ErrorDetailBuilder title(String title) {
      this.title = title;
      return this;
    }

    public ErrorDetailBuilder status(Integer status) {
      this.status = status;
      return this;
    }

    public ErrorDetailBuilder detail(String detail) {
      this.detail = detail;
      return this;
    }

    public ErrorDetailBuilder timestamp(Long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    public ErrorDetailBuilder developerMessage(String developerMessage) {
      this.developerMessage = developerMessage;
      return this;
    }

    public ErrorDetail build() {
      ErrorDetail errorDetail = new ErrorDetail();
      errorDetail.setTitle(title);
      errorDetail.setStatus(status);
      errorDetail.setDetail(detail);
      errorDetail.setTimestamp(timestamp);
      errorDetail.setDeveloperMessage(developerMessage);
      return errorDetail;
    }
  }
}
