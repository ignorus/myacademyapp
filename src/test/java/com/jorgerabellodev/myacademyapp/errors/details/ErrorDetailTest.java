package com.jorgerabellodev.myacademyapp.errors.details;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jorgerabellodev.myacademyapp.errors.details.ErrorDetail.ErrorDetailBuilder;
import org.junit.jupiter.api.Test;

class ErrorDetailTest {

  @Test
  public void whenTryToCreateANewBuilderShouldReturnAnInstanceOfErrorDetailBuilder() {

    ErrorDetailBuilder builder = ErrorDetailBuilder.newBuilder();
    assertNotNull(builder);
  }

}
