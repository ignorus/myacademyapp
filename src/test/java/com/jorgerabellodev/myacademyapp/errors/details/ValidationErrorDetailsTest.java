package com.jorgerabellodev.myacademyapp.errors.details;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import com.jorgerabellodev.myacademyapp.errors.details.ValidationErrorDetails.Builder;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

class ValidationErrorDetailsTest {

  private Long now;

  @BeforeEach
  public void beforeEach() {
    now = new Date().getTime();
  }

  @Test
  public void whenTryToCreateANewBuilderShouldReturnAnInstanceOfValidationErrorDetailsBuilder() {

    Builder builder = Builder.newBuilder();
    assertNotNull(builder);
  }

  @Test
  public void whenTryToCreateANewAlreadyRegistredStudentDetailsShouldReturnAnInstanceOfValidationErrorDetails() {

    ValidationErrorDetails vedExceptionDetails = ValidationErrorDetails.Builder.newBuilder()
        .timestamp(now)
        .status(HttpStatus.BAD_REQUEST.value())
        .title("Field validation error")
        .detail("Field validation error")
        .developerMessage(any())
        .field(any())
        .fieldMessage(any())
        .build();

    assertNotNull(vedExceptionDetails);
    assertEquals(now, vedExceptionDetails.getTimestamp());
    assertEquals(400, vedExceptionDetails.getStatus());
    assertEquals("Field validation error", vedExceptionDetails.getTitle());

  }
}
