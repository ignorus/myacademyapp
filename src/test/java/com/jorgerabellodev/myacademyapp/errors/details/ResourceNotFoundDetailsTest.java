package com.jorgerabellodev.myacademyapp.errors.details;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import com.jorgerabellodev.myacademyapp.errors.details.ResourceNotFoundDetails.Builder;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

class ResourceNotFoundDetailsTest {

  private Long now;

  @BeforeEach
  public void beforeEach() {
    now = new Date().getTime();
  }

  @Test
  public void whenTryToCreateANewBuilderShouldReturnAnInstanceOfResourceNotFoundDetailsBuilder() {

    Builder builder = Builder.newBuilder();
    assertNotNull(builder);
  }

  @Test
  public void whenTryToCreateANewAlreadyRegistredStudentDetailsShouldReturnAnInstanceOfResourceNotFoundDetails() {

    ResourceNotFoundDetails rfnExceptionDetails = ResourceNotFoundDetails.Builder.newBuilder()
        .timestamp(now)
        .status(HttpStatus.NOT_FOUND.value())
        .title("Resource not found")
        .detail(any())
        .developerMessage(any())
        .build();

    assertNotNull(rfnExceptionDetails);
    assertEquals(now, rfnExceptionDetails.getTimestamp());
    assertEquals(404, rfnExceptionDetails.getStatus());
    assertEquals("Resource not found", rfnExceptionDetails.getTitle());

  }

}
