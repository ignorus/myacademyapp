package com.jorgerabellodev.myacademyapp.services;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.jorgerabellodev.myacademyapp.errors.exceptions.AlreadyRegistredStudentException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.EmptyStudentDataSetException;
import com.jorgerabellodev.myacademyapp.errors.exceptions.ResourceNotFoundException;
import com.jorgerabellodev.myacademyapp.models.dtos.request.NewStudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.request.StudentRequestDTO;
import com.jorgerabellodev.myacademyapp.models.dtos.response.StudentResponseDTO;
import com.jorgerabellodev.myacademyapp.models.entities.Student;
import com.jorgerabellodev.myacademyapp.repositories.StudentRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

  @InjectMocks
  private StudentService service;

  @Mock
  private StudentRepository repository;

  @Mock
  private ModelMapper modelMapper;

  @BeforeEach
  public void setup() {
    service = new StudentService(repository, modelMapper);
  }

  @Test
  public void givenAStudentWhenTryToSaveShouldReturnTheSavedStudent() {
    NewStudentRequestDTO newStudentRequestDTO = new NewStudentRequestDTO("Jorge", "jorge@mail.com");
    Student newStudent = new Student(1L, "Jorge", "jorge@mail.com");
    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge", "jorge@mail.com");

    when(repository.save(any())).thenReturn(newStudent);

    when(modelMapper.map(newStudentRequestDTO, Student.class)).thenReturn(newStudent);
    when(modelMapper.map(newStudent, StudentResponseDTO.class)).thenReturn(studentResponseDTO);

    StudentResponseDTO savedStudent = service.save(newStudentRequestDTO);

    assertNotNull(savedStudent);
    assertEquals(1L, savedStudent.getId());
    assertEquals("Jorge", savedStudent.getName());
    assertEquals("jorge@mail.com", savedStudent.getEmail());
  }

  @Test
  public void whenTryToRetrieveAllSavedStudentsShouldReturnAllTheSavedStudents() {

    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge", "jorge@email.com");
    Student student = new Student(1L, "Jorge", "jorge@email.com");

    List<Student> students = asList(
        new Student(1L, "Jorge", "jorge@email.com"),
        new Student(2L, "Maria", "maria@email.com"),
        new Student(3L, "Eduarda", "eduarda@email.com"),
        new Student(4L, "José", "jose@email.com"),
        new Student(5L, "João", "joao@email.com")
    );

    when(repository.findAll()).thenReturn(students);

    when(modelMapper.map(student, StudentResponseDTO.class)).thenReturn(studentResponseDTO);

    List<StudentResponseDTO> everybody = service.findAll();

    assertNotNull(everybody);
    assertNotNull(everybody);
    assertFalse(everybody.isEmpty());
    assertEquals("Jorge", everybody.get(0).getName());
    assertEquals("jorge@email.com", everybody.get(0).getEmail());

  }

  @Test
  public void whenTryToRetrieveAllSavedStudentsAndTheresAreNoSavedStudentsShouldEmptyDatasetException() {

    List<Student> emptyList = new ArrayList<>();

    when(repository.findAll()).thenReturn(emptyList);

    assertThrows(EmptyStudentDataSetException.class, () -> service.findAll());

  }

  @Test
  public void givenAStudentIDAndTheIDExistsWhenTryToRetrieveASavedStudentShouldReturnTheSavedStudent() {
    Student student = new Student(1L, "Jorge", "jorge@mail.com");
    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge", "jorge@mail.com");

    when(repository.existsById(1L)).thenReturn(true);
    when(repository.getOne(1L)).thenReturn(student);

    when(modelMapper.map(student, StudentResponseDTO.class)).thenReturn(studentResponseDTO);

    StudentResponseDTO foundStudent = service.findById(1L);

    assertNotNull(foundStudent);
    assertEquals(1, foundStudent.getId());
    assertEquals("Jorge", foundStudent.getName());
    assertEquals("jorge@mail.com", foundStudent.getEmail());
  }

  @Test
  public void givenAStudentIDAndTheIDDoesNotExistsWhenTryToRetrieveASavedStudentShouldThrowStudentNotFoundException() {
    when(repository.existsById(100L)).thenReturn(false);
    assertThrows(ResourceNotFoundException.class, () -> service.findById(100L));
  }

  @Test
  public void givenAStudentIdWhenTryToDeleteASavedStudentShouldRemoveItFromDataBase() {
    when(repository.existsById(1L)).thenReturn(true);
    service.delete(1L);
  }

  @Test
  public void givenAStudentIdThatDoesNotExistsWhenTryToDeleteASavedStudentShouldThrowStudentNotFoundException() {
    when(repository.existsById(1L)).thenReturn(false);
    assertThrows(ResourceNotFoundException.class, () -> service.delete(1L));
  }

  @Test
  public void givenAStudentPayloadWithAnIDWhenTryToUpdateThisAttributesShouldReturnTheUpdatedStudent() {

    StudentRequestDTO studentRequestDTO = new StudentRequestDTO(1L, "Jorge", "jorge@mail.com");
    Student student = new Student(1L, "Jorge", "jorge@mail.com");

    Student newStudent = new Student(1L, "Jorge Rabello", "jorge.rabello@mail.com");
    StudentResponseDTO studentResponseDTO = new StudentResponseDTO(1L, "Jorge Rabello",
        "jorge.rabello@mail.com");

    when(modelMapper.map(studentRequestDTO, Student.class)).thenReturn(student);
    when(modelMapper.map(newStudent, StudentResponseDTO.class)).thenReturn(studentResponseDTO);
    when(repository.existsById(studentRequestDTO.getId())).thenReturn(true);

    when(repository.save(student)).thenReturn(newStudent);

    StudentResponseDTO updatedStudent = service.update(studentRequestDTO);

    assertNotNull(updatedStudent);
    assertEquals(1, updatedStudent.getId());
    assertEquals("Jorge Rabello", updatedStudent.getName());
    assertEquals("jorge.rabello@mail.com", updatedStudent.getEmail());

  }

  @Test
  public void givenAStudentIdThatDoesNotExistsWhenTryToUpdateASavedStudentShouldThrowStudentNotFoundException() {
    StudentRequestDTO studentRequestDTO = new StudentRequestDTO(1L, "Jorge", "jorge@mail.com");
    when(repository.existsById(studentRequestDTO.getId())).thenReturn(false);
    assertThrows(ResourceNotFoundException.class, () -> service.update(studentRequestDTO));
  }

  @Test
  public void givenAStudentwhoseEmailIsAlreadyRegisteredWhenTryToSaveShouldThrowAlreadyRegistredStudentException() {
    NewStudentRequestDTO newStudentRequestDTO = new NewStudentRequestDTO("Jorge", "jorge@mail.com");
    Student newStudent = new Student(1L, "Jorge", "jorge@mail.com");

    when(repository.findByEmail("jorge@mail.com")).thenReturn(newStudent);
    when(modelMapper.map(newStudentRequestDTO, Student.class)).thenReturn(newStudent);

    assertThrows(AlreadyRegistredStudentException.class, () -> service.save(newStudentRequestDTO));

  }
}
